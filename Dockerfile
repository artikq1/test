FROM maven:3.9.6-ibm-semeru-17-focal AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests

FROM openjdk:17-alpine
WORKDIR /app
COPY --from=build /app/target/weather-service-*.jar ./weather-service.jar
CMD ["java", "-jar", "weather-service.jar"]
